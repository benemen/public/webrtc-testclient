# WebRTC Testclient

This sample client is one example of how to embed WebRTC phone into your webpage using iframe HTML element. Click the following link to see the sample client in action,

https://benemen.gitlab.io/public/webrtc-testclient/

## Embedding WebRTC phone

Take the **webrtc-client-ui.js** or **webrtc-client-ui.min.js** (depends on which version you use - development or production) and embed it as a regular javascript file. It should contain all needed files, even images, and icons, so it's the only one file that you have to embed to your project.

      <script type="text/javascript" src="webrtc-client-ui.js"></script>

And place the WebRTC phone UI web component as any other HTML DOM element.

      <webrtc-client-ui></webrtc-client-ui>

Client versions can be found from https://phone.enreachvoice.com/versions.html. For example version 2024.2.0 can be accessed from https://phone.enreachvoice.com/2024.2.0/webrtc-client-ui.min.js. Release notes for the WebRTC phone are published [here](https://benemen.atlassian.net/wiki/spaces/PD/pages/899088893/Voice+for+Browser+release+notes).

### Optional: Setting client id

You can specify embedding client via client-id attribute. This is for SSO login and its default value is **voice-for-browser**.

      <webrtc-client-ui client-id="some-id"></webrtc-client-ui>

### Optional: Setting discovery url

**_Version 2023.3.2 and later._**

You can specify discovery service using discovery-url attribute. This is mainly used for development purposes with test users. Doesn't need to be set if you aren't using some test setup.

      <webrtc-client-ui discovery-url="https://discover.enreachvoice.com/api/user/"></webrtc-client-ui>

### Optional: Setting PNPS mission type

**_Version 2024.3.0 and later._**

You can specify PNPS mission type using pnps-mission-type attribute. This is for the PNPS queries WebRTC phone periodically asks. Mission type determines the context for the queries which can have different questions depending on the type. Available values are 'default', 'standalone', 'd365' and 'salesforce'. This is targeted for special use cases and doesn't need to be configured. If not configured 'default' is used.

      <webrtc-client-ui pnps-mission-type="salesforce"></webrtc-client-ui>

If you don't want WebRTC phone to periodically ask PNPS queries, you can disable it by using disable-pnps-query attribute.

      <webrtc-client-ui disable-pnps-query></webrtc-client-ui>

## Authentication

To use WebRTC phone you have to authenticate in BeneAPI or alternatively use a custom proxy between client and BeneAPI. Check possible ways below,

#### Login form in UI

Default option is to display login form which requires username and password to authenticate in BeneAPI. It's the easiest way to authenticate and start using WebRTC phone.

#### Public API method: **_loginUsingUsernameAndPassword()_**

WebRTC phone allows users to omit authentication using login form. The second auth option is to use public method called **loginUsingUsernameAndPassword()** to supply username and password.

#### Public API method: **_loginUsingAuthHeader()_**

A third authentication possibility is through another public method exposed by WebRTC phone **loginUsingAuthHeader()**. Use it to supply JWT token, user id and email for authentication.

#### Public API method: **_loginUsingSSO()_**

Final option for authentication is to use SSO through Enreach Identity or some third party solution such as Azure AD. You can use this authentication via **loginUsingSSO()** public method supplying only username.

## Public API

WebRTC client UI has exposed some public methods and events to be used outside UI. Descriptions of both methods and events is found below,

### Methods

#### **answerCall()**

##### **(callId: string): void**

Allows answering the call from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **call()**

##### **(targetNumber: string): Promise**

Allows starting call from outside. Returns Promise with started calls id. Call id is also available via call events.

|   **Name**   | **Type** |       **Description**       |   **Example**   |
| :----------: | :------: | :-------------------------: | :-------------: |
| targetNumber |  string  | Phone number where to call. | '+358293000988' |

<br>

#### **call()**

##### **(targetNumber: string, outboundServiceCallData?: OutboundServiceCallDataInterface): Promise**

**_Version 2024.3.0 and later._**

Allows starting call from outside. Returns a Promise with started calls id. Call id is also available via call events. Providing optional parameter for outbound service calls will override currently selected role via UI for the call.

|        **Name**         |             **Type**             |                   **Description**                   |                     **Example**                     |
| :---------------------: | :------------------------------: | :-------------------------------------------------: | :-------------------------------------------------: |
|      targetNumber       |              string              |             Phone number where to call.             |                   '+358293000988'                   |
| outboundServiceCallData | OutboundServiceCallDataInterface | _Optional_. Override UI selected role for the call. | { queueId: '05dd1655-5ff2-4612-93fa-2a2d1b19c925' } |

**OutboundServiceCallDataInterface**

|     **Name**      | **Type** |                       **Description**                       |              **Example**               |
| :---------------: | :------: | :---------------------------------------------------------: | :------------------------------------: |
|  callbackListId   | string?  |  _Optional_. Id of the callback list the call relates to.   | '78c79e79-32c7-4a76-9513-6fc5ca0227d6' |
| callbackRequestId | string?  | _Optional_. Id of the callback request the call relates to. | 'a014b3ba-1569-4798-b699-98b9601fe291' |
|      queueId      | string?  |      _Optional_. Id of the queue the call relates to.       | '05dd1655-5ff2-4612-93fa-2a2d1b19c925' |
|        cli        | string?  | _Optional_. Calling Line Identification (CLI) for the call. |            '+358123456789'             |

<br>

#### **callOutboundCallback()**

##### **(targetNumber: string, callbackListId: string, callbackRequestId: string, cli?: string): Promise**

**_Version 2024.2.0 and later._**
**_DEPRICATED: Recommended to use call() method with optional data object_**

Allows starting an service call instead of direct call on behalf of a callback list. Returns Promise with started call's id. Call id is also available via call events.

|     **Name**      | **Type** |                       **Description**                       |              **Example**               |
| :---------------: | :------: | :---------------------------------------------------------: | :------------------------------------: |
|   targetNumber    |  string  |        Phone number where to call the service call.         |            '+358293000988'             |
|  callbackListId   |  string  |        Id of the callback list the call relates to.         | '78c79e79-32c7-4a76-9513-6fc5ca0227d6' |
| callbackRequestId |  string  |       Id of the callback request the call relates to.       | 'a014b3ba-1569-4798-b699-98b9601fe291' |
|        cli        |  string  | _Optional_. Calling Line Identification (CLI) for the call. |            '+358123456789'             |

<br>

#### **callOutboundQueue()**

##### **(targetNumber: string, queueId: string, cli?: string): Promise**

**_Version 2024.2.0 and later._**
**_DEPRICATED: Recommended to use call() method with optional data object_**

Allows starting an service call instead of direct call on behalf of a queue. Returns Promise with started call's id. Call id is also available via call events.

|   **Name**   | **Type** |                       **Description**                       |              **Example**               |
| :----------: | :------: | :---------------------------------------------------------: | :------------------------------------: |
| targetNumber |  string  |        Phone number where to call the service call.         |            '+358293000988'             |
|   queueId    |  string  |            Id of the queue the call relates to.             | '05dd1655-5ff2-4612-93fa-2a2d1b19c925' |
|     cli      |  string  | _Optional_. Calling Line Identification (CLI) for the call. |            '+358123456789'             |

<br>

#### **confirmTransfer()**

##### **(): Promise**

**_Version 1.8.3 and later._**

Allows user to confirm call transfer from outside during consultation call. Consulted transfer must be confirmed to successfully complete transfer. Returns Promise containing following data,

**CallTransferStateInterface**

|   **Name**    | **Type** |                       **Description**                        |      **Example**       |
| :-----------: | :------: | :----------------------------------------------------------: | :--------------------: |
|    callId     |  string  |      Identifier for the call that is being transferred.      | 'ahmh78im1qstcilmif6n' |
|   confirmed   | boolean  | Indicates if transfer is completed from the UIs perspective. |          true          |
| transferredTo | string?  | _Optional_. Phone number where the call was transferred to.  |    '+358293000988'     |

<br>

#### **endCall()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows ending the call from outside. Similar to **rejectCall()** method.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **getUserRole()**

##### **(): Promise**

**_Version 2024.3.0 and later._**

Allows users to find their currently selected role. Returns a Promise containing following data (which is also exposed through user-role-changed events),

|    **Name**    |         **Type**          |                                                **Description**                                                |                                 **Example**                                 |
| :------------: | :-----------------------: | :-----------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------: |
|      Name      |          string           |                   Name of the role i.e. service queue name or 'personal' for personal role.                   |                                'test queue'                                 |
|    Numbers     | UserRoleNumberInterface[] |                                  All available display numbers for the role.                                  |                                   [ ... ]                                   |
| SelectedNumber |  UserRoleNumberInterface  |                                Currently selected display number for the role.                                | { Default: true, DisplayNumber: '+358293000988', DisplayName: 'test queue'} |
|    QueueId     |          string?          | _Optional_. Service queue identification for associated service role. Populated only if role is service role. |                   '05dd1655-5ff2-4612-93fa-2a2d1b19c925'                    |
|    Recorded    |         boolean?          |                                     _Optional_. If the call is recorded.                                      |                                    false                                    |

**UserRoleNumberInterface**

|   **Name**    | **Type** |            **Description**             |   **Example**   |
| :-----------: | :------: | :------------------------------------: | :-------------: |
|    Default    | boolean  | If the number is default for the role. |      true       |
| DisplayNumber |  string  |            Display number.             | '+358293000988' |
|  DisplayName  |  string  |      Display name for the number.      |  'test queue'   |

<br>

#### **getUserStatus()**

##### **(): Promise**

Allows users to find their current availability from outside. Returns Promise containing following data,

|      **Name**       |          **Type**           |                          **Description**                          |              **Example**               |
| :-----------------: | :-------------------------: | :---------------------------------------------------------------: | :------------------------------------: |
|       EndDate       |           string?           |        _Optional_. Availability event specified end date.         |       '2022-12-01T09:01:45.613Z'       |
| EventSourceCategory |   EventSourceCategoryEnum   |                    Event source category name.                    |                 'User'                 |
|     EventTypeID     | AvailabilityEventTypeIdEnum |                         Event main type.                          |                   2                    |
|         Id          |           string?           |        _Optional_. Identifier for the availability event.         | 'fc7536af-5471-ed11-993c-005056895f22' |
|        Note         |           string            |                     User defined event note.                      |              'Some note'               |
|   PresenceTypeID    |     PresenceTypeIdEnum      |                       Event presence type.                        |                   0                    |
|      StartDate      |           string?           |            _Optional_. Availability event start date.             |       '2022-12-01T08:46:45.787Z'       |
|      Timestamp      |           string?           |             _Optional_. When event was last changed.              |       '2022-12-01T08:46:45.880Z'       |
|   TransferNumber    |           string?           | _Optional_. Number where incoming calls should be transferred to. |            '+358293000988'             |
|      WrapUpEnd      |           string?           | _Optional_. Timestamp indicating when wrap-up ends automatically. |       '2022-12-01T08:46:45.880Z'       |

<br>

#### **login()**

##### **(options: LoginOptionsInterface): Promise** 

Allows user to be signed in using any of the available login types; Credentials, AuthHeader, and SSO. The method returns an error if user has already signed in or required data is missing from the options object. Otherwise it will return true indicating successful login or an API error.

|  **Name**  |       **Type**       |                                                                                               **Description**                                                                                                |       **Example**       | **Version availability** |
| :--------: | :------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :---------------------: | :----------------------: |
|    type    |    LoginTypeEnum     |      Indicates which of the supported login types is to be used. Available values are 'Credentials', 'AuthHeader', and 'SSO'. Login type determines which of the other fields are required or optional.      |      'AuthHeader'       |    2024.5.1 and later    |
|  username  |       string?        |                                                           User’s name/email. Required by **Credentials**, **AuthHeader**, and **SSO** login types.                                                           |   'user@enreach.com'    |    2024.5.1 and later    |
|   userId   |       string?        |                                                                  _Optional_. User’s identification. Required by **AuthHeader** login type.                                                                   |     '17fd100e-7f80'     |    2024.5.1 and later    |
| authHeader | AuthHeaderInterface? |                                                               _Optional_. User’s authorization header. Required by **AuthHeader** login type.                                                                | { AuthHeaderInterface } |    2024.5.1 and later    |
|  password  |       string?        |                                                                     _Optional_. User’s password. Required by **Credentials** login type.                                                                     |        '123456'         |    2024.5.1 and later    |
| isInIframe |       boolean?       |       _Optional_. Alters the behavior of the phone to better suit embedded scenarios. If true permissions page will not show if not forced and browser notifications are disabled. Defaults to false.        |          true           |    2024.5.1 and later    |
|  siloName  |       string?        |                                                                       _Optional_. User is signed in to the provided silo if possible.                                                                        |      'TestSilo01'       |    2024.5.1 and later    |
|  keycloak  |      Keycloak?       | _Optional_. Keycloak object to be used the sign in process. Allows the phone to sign in without reloading the page. The object must be authenticated before calling the method. Optional for SSO login type. |      { Keycloak }       |    2024.5.1 and later    |

**AuthHeaderInterface**

| **Name**  | **Type** |                         **Description**                          |                     **Example**                     |
| :-------: | :------: | :--------------------------------------------------------------: | :-------------------------------------------------: |
|   value   |  string  |                   Token type and token value.                    | 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNlOWMxNTA...' |
| expiresIn |  number  | Validity time for impersonation token (have to be more than 60). |                         600                         |

<br>

#### **loginUsingAuthHeader()**

##### **(authHeader: BeneApiAuthHeaderInterface, beneUserId: string, beneUserEmail: string, isInIFrame?: boolean, siloName?: string): Promise**

**_DEPRICATED: Recommended to use login() method with type 'AuthHeader'_**

Allows to sign in using JWT token and use custom proxy. Returns Promise.

|   **Name**    |          **Type**          |                                                                     **Description**                                                                     |                                **Example**                                 | **Version availability** |
| :-----------: | :------------------------: | :-----------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------: | :----------------------: |
|  authHeader   | BeneApiAuthHeaderInterface |                                                     Authentication header used in BeneAPI requests.                                                     | { 'value: 'Bearer FG4ypQwBnZZQE9uAB0vOoEIn3rIctNthZwJ, 'expiresIn: '600' } |            -             |
|  beneUserId   |           string           |                                           Identifier that will be used as user context for BeneAPI requests.                                            |                   '17fd100e-7f80-e911-80d4-00505689257e'                   |            -             |
| beneUserEmail |           string           |                                    User email, that will be attached to logs and used to make API discovery request.                                    |                             'anyuser@bene.com'                             |            -             |
|  isInIFrame   |          boolean?          | _Optional_. Set to true if the application is embedded in an iframe, which requires different behavior (permissions). Default value is set to **true**. |                                   false                                    |            -             |
|   siloName    |          string?           |                       _Optional_. Set specific silo name to which the user is logged into. Default value is set to **undefined**.                       |                                'TestSilo01'                                |    2024.4.1 and later    |

**BeneApiAuthHeaderInterface**

| **Name**  | **Type** |                         **Description**                          |                     **Example**                     |
| :-------: | :------: | :--------------------------------------------------------------: | :-------------------------------------------------: |
|   value   |  string  |                   Token type and token value.                    | 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNlOWMxNTA...' |
| expiresIn |  number  | Validity time for impersonation token (have to be more than 60). |                         600                         |

<br>

#### **loginUsingSSO()**

##### **(username: string, isInIFrame?: boolean, keycloak?: Keycloak, siloName?: string): Promise**

**_DEPRICATED: Recommended to use login() method with type 'SSO'_**

Allows to sign in using SSO through Enreach Identity or other third party service. Returns Promise. If SSO authentication is not in use, returned Promise has boolean value of **false** as a result.

|  **Name**  | **Type**  |                                                                      **Description**                                                                       |     **Example**     | **Version availability** |
| :--------: | :-------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------: | :----------------------: |
|  username  |  string   |                                                                     BeneAPI username.                                                                      | 'beneuser@bene.com' |            -             |
| isInIFrame | boolean?  |  _Optional_. Set to true if the application is embedded in an iframe, which requires different behavior (permissions). Default value is set to **true**.   |        false        |            -             |
|  keycloak  | Keycloak? | _Optional_. Provide already authenticated Keycloak object. Otherwise, it's automatically created and authenticated. Default value is set to **undefined**. |  Keycloak { ... }   |    2023.2.2 and later    |
|  siloName  |  string?  |                        _Optional_. Set specific silo name to which the user is logged into. Default value is set to **undefined**.                         |    'TestSilo01'     |    2024.4.1 and later    |

<br>

#### **loginUsingUsernameAndPassword()**

##### **(username: string, password: string, isInIFrame?: boolean, siloName?: string): Promise**

**_DEPRICATED: Recommended to use login() method with type 'Credentials'_**

Allows to sign in using BeneAPI username and password. Returns Promise.

|  **Name**  | **Type** |                                                                     **Description**                                                                      |     **Example**     | **Version availability** |
| :--------: | :------: | :------------------------------------------------------------------------------------------------------------------------------------------------------: | :-----------------: | :----------------------: |
|  username  |  string  |                                                                    BeneAPI username.                                                                     | 'beneuser@bene.com' |            -             |
|  password  |  string  |                                                                    BeneAPI password.                                                                     | '123321azxxcasdae1' |            -             |
| isInIFrame | boolean? | _Optional_. Set to true if the application is embedded in an iframe, which requires different behavior (permissions). Default value is set to **false**. |        true         |            -             |
|  siloName  | string?  |                       _Optional_. Set specific silo name to which the user is logged into. Default value is set to **undefined**.                        |    'TestSilo01'     |    2024.4.1 and later    |

<br>

#### **logout()**

##### **(): void**

**_Version 1.8.3 and later._**

Allows to sign out from WebRTC phone.

<br>

#### **logout()**

##### **(force?: boolean): void**

**_Version 2024.3.0 and later._**

Allows to sign out from WebRTC phone.

| **Name** | **Type** |                                    **Description**                                     | **Example** |
| :------: | :------: | :------------------------------------------------------------------------------------: | :---------: |
|  force   | boolean? | If set to true, user is signed out without alert prompt notifying user of page reload. |    true     |

<br>

#### **makeAPICall()**

##### **(method: APICallTypeEnum, relativePath: string, payload?: object): Promise**

Allows signed in users to make API calls programmatically. Returns error if user is not signed in or tries to use unsupported HTTP method otherwise returned Promise contains API response.

|   **Name**   |     **Type**     |                                                 **Description**                                                  |      **Example**       |
| :----------: | :--------------: | :--------------------------------------------------------------------------------------------------------------: | :--------------------: |
|    method    | APICallTypeEnuum |               Used HTTP method. Currently supported methods are 'GET', 'POST', 'PUT' and 'DELETE'.               |         'POST'         |
| relativePath |      string      | Used resource path. It can include '[ME]' (case-insensitive) placeholder which will be raplaced by users own id. | 'users/[me]/settings/' |
|   payload    |     object?      |                                       _Optional._ Payload for the request.                                       |   { 'some': 'data' }   |

<br>

#### **muteCall()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows muting the call from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **pauseCall()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows putting the call on hold from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **rejectCall()**

##### **(callId: string): void**

Allows rejecting the call from outside. Similar to **endCall()** method.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **resumeCall()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows resuming the call put on hold from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **setAudioHandling()**

##### **(type: AudioHandlingEnum): Promise**

**_Version 2023.2.6 and later._**

Select between legacy or default audio handling. Can be used before or after login. Returns a Promise containing boolean value true when setup is complete.

Legacy audio handling keeps audio streams always open while default audio handling closes them when not needed. Version 2023.3.2 onwards audio handing can also be selected from phone UI and this will override programmatical audio handling changing. If user has chosen audio handling mode via UI this method gives an error informing that mode cannot be changed because of user value.

| **Name** |     **Type**      |                                  **Description**                                   | **Example** |
| :------: | :---------------: | :--------------------------------------------------------------------------------: | :---------: |
|   type   | AudioHandlingEnum | Type of audio handling. Available options are string values 'legacy' and 'default' |  'legacy'   |

<br>

#### **setAuthHeader()**

##### **(authHeader: BeneApiAuthHeaderInterface): void**

Update authorization header.

|  **Name**  |          **Type**          |                 **Description**                 |                                **Example**                                 |
| :--------: | :------------------------: | :---------------------------------------------: | :------------------------------------------------------------------------: |
| authHeader | BeneApiAuthHeaderInterface | Authentication header used in BeneAPI requests. | { 'value: 'Bearer FG4ypQwBnZZQE9uAB0vOoEIn3rIctNthZwJ, 'expiresIn: '600' } |

**BeneApiAuthHeaderInterface**

| **Name**  | **Type** |                         **Description**                          |                     **Example**                     |
| :-------: | :------: | :--------------------------------------------------------------: | :-------------------------------------------------: |
|   value   |  string  |                   Token type and token value.                    | 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjNlOWMxNTA...' |
| expiresIn |  number  | Validity time for impersonation token (have to be more than 60). |                         600                         |

<br>

#### **setAuthType()**

##### **(authType: AuthTypeEnum): void**

Set authorization type (currently only changes login form into welcome screen in UI).

| **Name** |   **Type**   |                                               **Description**                                               | **Example** |
| :------: | :----------: | :---------------------------------------------------------------------------------------------------------: | :---------: |
| authType | AuthTypeEnum | Type of authentication method. Available values are 'loginForm' and 'JWT'. Default is set to **loginForm**. |    'JWT'    |

<br>

#### **setClientId()**

##### **(id: string): Promise**

Set client-id attribute for SSO login. Default value is **voice-for-browser**. Returns Promise.

| **Name** | **Type** |                        **Description**                         |     **Example**     |
| :------: | :------: | :------------------------------------------------------------: | :-----------------: |
|    id    |  string  | Identification for embedded phone when using SSO login method. | 'voice-for-browser' |

<br>

#### **setDiscoveryUrl()**

##### **(url: string): Promise**

**_Version 2023.3.2 and later._**

Set discovery service. This is mainly used for development purposes with test users. Doesn't need to be set if you aren't using some test setup. Returns Promise.

| **Name** | **Type** |          **Description**          |                  **Example**                  |
| :------: | :------: | :-------------------------------: | :-------------------------------------------: |
|   url    |  string  | Used discovery service's address. | 'https://discover.enreachvoice.com/api/user/' |

<br>

#### **setPNPSMissionType()**

##### **(type: PNPSMissionTypeEnum): void**

**_Version 2024.3.0 and later._**

Set PNPS mission type for the PNPS query. Type can also be configured with pnps-mission-type HTML attribute. Must be set before sign in.

| **Name** |      **Type**       |                                                               **Description**                                                                | **Example**  |
| :------: | :-----------------: | :------------------------------------------------------------------------------------------------------------------------------------------: | :----------: |
|   type   | PNPSMissionTypeEnum | Set mission type for PNPS query. Accepted values are 'default', 'standalone', 'd365' and 'salesforce'. Value is set to 'default' by default. | 'salesforce' |

<br>

#### **setPhoneAlerts()**

##### **(enabled: boolean): void**

**_Version 1.8.3 and later._**

Allows toggling phone alerts i.e. ringtone on and off.

| **Name** | **Type** |                               **Description**                                | **Example** |
| :------: | :------: | :--------------------------------------------------------------------------: | :---------: |
| enabled  | boolean  | Determines if phone alerts are to be used. Value set to **true** by default. |    false    |

<br>

#### **setPhoneLanguage()**

##### **(lang: string): void**

Allows setting phone language from outside.

| **Name** | **Type** |                              **Description**                              | **Example** |
| :------: | :------: | :-----------------------------------------------------------------------: | :---------: |
|   lang   |  string  | Language code. Currently supported values/languages are 'en', 'fi', 'sv'. |    'en'     |

<br>

#### **setUserData()**

##### **(data: object): Promise**

Set custom data to be used in call transfers. In case of consulted transfers, data must be set **_before_** consultation call is started.

| **Name** | **Type** |                                  **Description**                                   |            **Example**            |
| :------: | :------: | :--------------------------------------------------------------------------------: | :-------------------------------: |
|   data   |  object  | Data object. Data length must be under 491 characters in serialized string format. | { 'WorkSpaceId': '928374912321' } |

<br>

#### **setUserRole()**

##### **(queueId?: string, number?: string): Promise**

**_Version 2024.3.0 and later._**

Set user role via method. Leaving both parameters undefined will select default role with default number for the user. Specifying only queueId will select the corresponding service role with the default number for the role. Specifying only number will choose personal role with the given number. Returns a Promise with boolean value indicating whether the role change was successful (true) or not (false). If specified values for queueId or number are invalid, role is not changed. Role changes are notified by events sent by WebRTC phone.

| **Name** | **Type** |                         **Description**                         |              **Example**               |
| :------: | :------: | :-------------------------------------------------------------: | :------------------------------------: |
| queueId  | string?  | _Optional_. Queue identification which determines service role. | '05dd1655-5ff2-4612-93fa-2a2d1b19c925' |
|  number  | string?  |             _Optional_. Display number to be used.              |            '+358293000988'             |

<br>

#### **startCallRecording()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows starting call recording from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **stopCallRecording()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows stopping call recording from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **transferCall()**

##### **(transferType: TransferTypeEnum, transferToNumber: string): Promise**

**_Version 1.8.3 and later._**

Allows direct or consulted call transfers from outside.

|     **Name**     |     **Type**     |                                                                              **Description**                                                                               |   **Example**   |
| :--------------: | :--------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-------------: |
|   transferType   | TransferTypeEnum |                                                    Type of call transfer. Available types are 'direct' or 'consulted'.                                                     |    'direct'     |
| transferToNumber |      string      | Phone number where to transfer the call either directly or after consulted call. In the case of consulted transfer this is the number where to make the consultation call. | '+358293000988' |

Returns Promise containing following transfer status data,

**CallTransferStateInterface**

|    **Name**     | **Type** |                       **Description**                        |      **Example**       |
| :-------------: | :------: | :----------------------------------------------------------: | :--------------------: |
|     callId      |  string  |      Identifier for the call that is being transferred.      | 'ahmh78im1qstcilmif6n' |
|    confirmed    | boolean  | Indicates if transfer is completed from the UIs perspective. |          true          |
| consultedCallId | string?  |      _Optional_. Identifier for the consultation call.       | 'ahmh78im1qstcilmif6n' |
|  transferredTo  | string?  | _Optional_. Phone number where the call was transferred to.  |    '+358293000988'     |

<br>

#### **unmuteCall()**

##### **(callId: string): void**

**_Version 1.8.3 and later._**

Allows unmuting the call from outside.

| **Name** | **Type** |     **Description**      |      **Example**       |
| :------: | :------: | :----------------------: | :--------------------: |
|  callId  |  string  | Identifier for the call. | 'ahmh78im1qstcilmif6n' |

<br>

#### **updateBeneUiUrl()**

##### **(url: string): void**

Allows setting UI endpoint from outside. Internally, the passed URL is parsed using [this](https://developer.mozilla.org/en-US/docs/Web/API/URL/origin).

| **Name** | **Type** | **Description**  |                                **Example**                                |
| :------: | :------: | :--------------: | :-----------------------------------------------------------------------: |
|   url    |  string  | New UI endpoint. | 'https://benedesk-qa.beneservices.com/Account/LogOn/?un=user@company.com' |

<br>

#### **updateJWT()**

##### **(token: string): void**

JWT has validity period and sometimes you will have to renew the token. In such case, you can use this method to update token inside WebRTC phone.

| **Name** | **Type** | **Description** |        **Example**        |
| :------: | :------: | :-------------: | :-----------------------: |
|  token   |  string  | New JWT token.  | 'eyJhbGciOiJSUzI1NiIs...' |

<br>

#### **startCallSupervision()**

##### **(targetUserId: string): Promise**

**_Version 2025.1.0 and later._**

Starts a call supervision session for the target user by checking if the user is logged in, no call is in progress, and a targetUserId is provided. The method rejects with an error if a validation fails or an API error occurs, and resolves to true if the session starts successfully.

| **Name**       | **Type** | **Description**                                          |        **Example**        | **Version availability** |
| :------------: | :------: | :----------------------------------------------------: | :-----------------------: | :-----------------------: |
| targetUserId   |  string  | The ID of the user to start call supervision for.       | '17fd100e-7f80-e911-80d4-00505689257e' | 2025.1.0 and later       |

<br>

### Events

WebRTC client UI emits events that you can listen to and trigger custom actions. All events are instances of [CustomEvent](https://lit-element.polymer-project.org/guide/events) that comes from [LitElement](https://lit-element.polymer-project.org/). Below you can find a list of those events,

##### **init**

Emitted after app is initialized, before authentication. It's the best event that you can use to log in.

      webrtcClientUIInstance.addEventListener('init', () => console.log('app is ready'));

##### **logged-in**

Emitted after user has successfully logged in.

      webrtcClientUIInstance.addEventListener('logged-in', () => console.log('user has logged in'));

##### **user-role-changed**

**_Version 2024.3.0 and later._**

Emitted after users current role changed.

      webrtcClientUIInstance.addEventListener('user-role-changed', (e) => console.log(e));

Payload for the user-role-changed event can be found in **detail** property of the **CustomEvent**. Data object is descibed below and it's the same what **getUserRole()** method returns,

|    **Name**    |         **Type**          |                                                **Description**                                                |                                 **Example**                                 |
| :------------: | :-----------------------: | :-----------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------: |
|      Name      |          string           |                   Name of the role i.e. service queue name or 'personal' for personal role.                   |                                'test queue'                                 |
|    Numbers     | UserRoleNumberInterface[] |                                  All available display numbers for the role.                                  |                                   [ ... ]                                   |
| SelectedNumber |  UserRoleNumberInterface  |                                Currently selected display number for the role.                                | { Default: true, DisplayNumber: '+358293000988', DisplayName: 'test queue'} |
|    QueueId     |          string?          | _Optional_. Service queue identification for assosiated service role. Populated only if role is service role. |                   '05dd1655-5ff2-4612-93fa-2a2d1b19c925'                    |
|    Recorded    |         boolean?          |                                     _Optional_. If the call is recorded.                                      |                                    false                                    |

**UserRoleNumberInterface**

|   **Name**    | **Type** |            **Description**             |   **Example**   |
| :-----------: | :------: | :------------------------------------: | :-------------: |
|    Default    | boolean  | If the number is default for the role. |      true       |
| DisplayNumber |  string  |            Display number.             | '+358293000988' |
|  DisplayName  |  string  |      Display name for the number.      |  'test queue'   |

##### **user-status-changed**

Emitted after users current availability or wrap-up status changed.

      webrtcClientUIInstance.addEventListener('user-status-changed', (e) => console.log(e));

Payload for the user-status-changed event can be found in **detail** property of the **CustomEvent**. Data object is descibed below and it's the same what **getUserStatus()** method returns,

|      **Name**       |          **Type**           |                          **Description**                          |              **Example**               |
| :-----------------: | :-------------------------: | :---------------------------------------------------------------: | :------------------------------------: |
|       EndDate       |           string?           |        _Optional_. Availability event specified end date.         |       '2022-12-01T09:01:45.613Z'       |
| EventSourceCategory |   EventSourceCategoryEnum   |                    Event source category name.                    |                 'User'                 |
|     EventTypeID     | AvailabilityEventTypeIdEnum |                         Event main type.                          |                   2                    |
|         Id          |           string?           |        _Optional_. Identifier for the availability event.         | 'fc7536af-5471-ed11-993c-005056895f22' |
|        Note         |           string            |                     User defined event note.                      |              'Some note'               |
|   PresenceTypeID    |     PresenceTypeIdEnum      |                       Event presence type.                        |                   0                    |
|      StartDate      |           string?           |            _Optional_. Availability event start date.             |       '2022-12-01T08:46:45.787Z'       |
|      Timestamp      |           string?           |             _Optional_. When event was last changed.              |       '2022-12-01T08:46:45.880Z'       |
|   TransferNumber    |           string?           | _Optional_. Number where incoming calls should be transferred to. |            '+358293000988'             |
|      WrapUpEnd      |           string?           | _Optional_. Timestamp indicating when wrap-up ends automatically. |       '2022-12-01T08:46:45.880Z'       |

##### **auth-token-expired**

Emitted 60 seconds before auth token is expired. This gives enough time to update header before token is actually expired and invalid. Auth header should be renewed manually only if user is signed in using **loginUsingAuthHeader()** method. Update should be done via **setAuthHeader()** method.

If the token is expired, WebRTC phone starts sending the event again in response to failed API requests. The event will be invoked at most in 2 second intervals to mitigate spam.

      webrtcClientUIInstance.addEventListener('auth-token-expired', () => console.log('update header'));

##### **phone-error**

**_Version 2023.3.2 and later._**

Emitted if an error occurs in WebRTC phone that needs to be exposed outside.

      webrtcClientUIInstance.addEventListener('phone-error', (e) => console.log(e));

Payload for the phone-error event can be found in **detail** property of the **CustomEvent**. In the property, there is the name of the error occured. Below is descritions for the possible errors tied to this event,

|      **Name**       |                                                                                **Description**                                                                                | **Version availability** |
| :-----------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :----------------------: |
|  user-click-needed  | WebRTC phone cannot play audio (ringtone) when a call comes in. User needs to click the document (phone) to make it active in order to give browser permission to play audio. |    2023.3.2 and later    |
| user-click-resolved |              When **use-click-needed** issue is resolved i.e. user clicks the document this event is emitted and it indicates that issue is no longer in effect.              |    2023.3.2 and later    |
|   connection-lost   |             WebRTC phone lost connection to the servers and no phone calls can be made or received. After the event phone tries actively restore the connection.              |    2024.4.1 and later    |
|  connection-failed  |            When WebRTC phone stops actively trying to restore connection to the servers this event is emitted and it indicates that you should restart the phone.             |    2024.4.1 and later    |
| connection-restored |                      When WebRTC phone reconnects to the servers this evetn is emitted and it indicates that you can continue to use the phone normally.                      |    2024.4.1 and later    |

##### **restart-phone**

Emitted if connection to backend fails and it cannot be reconnected.

      webrtcClientUIInstance.addEventListener('restart-phone', () => console.log('restart phone'));

<br>

##### **call-supervising-state-changed**

**_Version 2025.1.0 and later._**

Emitted when the call supervision state changes. The detail property of the CustomEvent contains a CallSupervisingInterface object with the updated state and targetUserId.

      webrtcClientUIInstance.addEventListener('call-supervising-state-changed', () => console.log('call supervising state changed'));

**CallSupervisingInterface**

| **Name**       | **Type**                   | **Description**                                                                       | **Example**                           | **Version availability** |
|----------------|----------------------------|---------------------------------------------------------------------------------------|---------------------------------------|--------------------------|
| state        | CallSupervisingStateEnum | The current state of call supervision. Possible values: connecting, started, ended, failed. | 'started'                           | 2025.1.0 and later       |
| targetUserId | string                   | The ID of the user being supervised.                                                  | '17fd100e-7f80-e911-80d4-00505689257e' | 2025.1.0 and later       |


<br>

#### Call events

Call events is a group of events that are strictly related to calling process. Each of the event from this group contains similar payload, defined by the event type. Below you can find the overall payload object which can be found in **detail** property of **CustomEvent** and the list of the events.

|    **Name**    |   **Type**   |                                     **Description**                                      |                              **Example**                              |
| :------------: | :----------: | :--------------------------------------------------------------------------------------: | :-------------------------------------------------------------------: |
|     callId     |    string    |                       Identifier for the call given by FreeSwitch.                       |                        'ahmh78im1qstcilmif6n'                         |
| callDirection  | CallTypeEnum |           Indicates call direction. Value is either 'incoming' or 'outbound'.            |                              'incoming'                               |
|  phoneNumber   |    string    |                            Phone number of the interlocutor.                             |                            '+358293093803'                            |
|   startTime    |     Date     |                             Date and time when call started.                             | Thu Mar 05 2020 12:30:39 GMT+0100 (Central European Standard Time) {} |
|  isConsulted   |   boolean    |                   Indicates if the call is consultation or basic call                    |                                 false                                 |
|    endTime     |    Date?     |         _Optional_. Date and time when call ends. Defined only for ended calls.          | Thu Mar 05 2020 12:30:39 GMT+0100 (Central European Standard Time) {} |
|    queueId     |   string?    |            _Optional_. Benemen queue identifier. Defined only for pool calls.            |                '17fd100e-7f80-e911-80d4-00505689257e'                 |
|   queueName    |   string?    |               _Optional_. Benemen queue name. Defined only for pool calls.               |                             'queue name'                              |
| transferredTo  |   string?    | _Optional_. Phone number where call was transferred. Defined only for transferred calls. |                            '+358293093804'                            |
|    userData    |   object?    |         _Optional_. Custom field used in call transfers to send additional data.         |                   { 'WorkspaceId': '928374912321' }                   |
| wrapUpDuration |   string?    |                   _Optional_. Wrap-up time in seconds after call ends.                   |                                 "60"                                  |

<br>

##### **call-started**

Called right after call was started from WebRTC client UI.

##### **call-in-progress**

Called when an outgoing call is acknowledged by the backend to be going out.

##### **call-incoming**

Called right after call comes to WebRTC client UI.

##### **call-answered**

Called right after incoming call was answered.

##### **call-confirmed**

Called right after incoming or outbound call was confirmed.

##### **call-ended**

Called right after incoming or outbound call ended.

##### **call-paused**

Called right after call was paused in WebRTC client UI.

##### **call-resumed**

Called right after paused call was resumed in WebRTC client UI.

##### **call-muted**

Called right after call was muted in WebRTC client UI.

##### **call-unmuted**

Called right after paused call was unmuted in WebRTC client UI.

##### **call-transfer-started**

Called right after direct transfer or when consultation call starts. This is currently the only event **direct transfers** raise.

##### **call-transfer-cancel**

Called right after when consultation call ends if the transfer wasn't confirmed.

##### **call-transfer-success**

Called right after when consultation call ends if the transfer was confirmed.

### HTML

WebRTC client UI requires few HTML API features that are not fully implemented in all of the browsers. Below you can find the list of HTML features that has to be enabled in browser to make WebRTC client UI work correctly.

|        **Dependency**        |                 **Can I use**                 |                                      **Docs**                                       |
| :--------------------------: | :-------------------------------------------: | :---------------------------------------------------------------------------------: |
| HTMLMediaElement.setSinkId() | [Link](https://caniuse.com/#search=setsinkid) | [Link](https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/setSinkId) |
|  HTMLMediaElement.srcObject  | [Link](https://caniuse.com/#search=srcObject) | [Link](https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/srcObject) |
